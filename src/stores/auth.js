import { ref } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'

export const useAuthStore = defineStore('auth', () => {
    const isLoggedIn = ref(false)
    const user = ref(null)
    const apiUrl = import.meta.env.VITE_API_URL

    async function login(username, password) {
        try {
            const response = await axios.post('${apiUrl}/api/login', { username, password })
            if (response.data.message === 'Login successful') {
                
                isLoggedIn.value = true
                user.value =  username  // Assuming the response contains user information
                // You can store the user information in a more detailed object based on your API response
            } else {
                throw new Error('Login failed')
            }
        } catch (error) {
            console.error('Login error:', error)
            isLoggedIn.value = false
            user.value = null
        }
    }

    function logout() {
        isLoggedIn.value = false
        user.value = null
        console.log("Logout success")
        // Optionally, you can also clear the user's session or token here
    }

    return { isLoggedIn, user, login, logout }
})