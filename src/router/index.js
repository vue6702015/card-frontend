import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CardView from '../views/CardView.vue'
import OneCardView from '../views/OneCardView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/ProfileView.vue')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('../components/test.vue')
    },
    // {
    //   path: '/cart',
    //   name: 'cart',
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import('../views/CartView.vue')
    // },
    {
      path: '/card/:id', // Dynamic route for individual cards
      name: 'card',
      component: OneCardView,
      props: true, // This allows us to pass route params as props to the component
    }
  ]
})

export default router
