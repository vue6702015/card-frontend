import { ref } from 'vue';

export function useFetchData() {
 const data = ref([]);
 const error = ref(null);

 const fetchData = async (id) => {
    try {
      const response = await fetch(`http://localhost:5000/api/data/${id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const result = await response.json();
      data.value = result;
      console.log(data.value[0][0])
    } catch (err) {
      error.value = err.message;
    }
 };

 return { data, error, fetchData };
}
