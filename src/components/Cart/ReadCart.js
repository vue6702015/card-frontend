import { ref } from 'vue';
import axios from 'axios';

export function useFetchCartData() {
 const cartData = ref(null);
 const error = ref(null);

 const fetchCartData = async (username) => {
    try {
      const response = await axios.get(`http://localhost:5000/cart/${username}`);
      cartData.value = response.data;
    } catch (err) {
      error.value = err.response ? err.response.data.error : 'An error occurred';
    }
 };

 return { cartData, error, fetchCartData };
}
