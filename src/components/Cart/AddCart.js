import { ref } from 'vue';
import axios from 'axios';

export function useAddToCart() {
 const error = ref(null);
 const message = ref(null);

 const addToCart = async (data) => {
    try {
      const response = await axios.post('http://localhost:5000/cart', data);
      message.value = response.data.message;
    } catch (err) {
      error.value = err.response ? err.response.data.error : 'An error occurred';
    }
 };

 return { error, message, addToCart };
}