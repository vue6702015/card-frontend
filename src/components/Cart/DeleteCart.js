import { ref } from 'vue';
import axios from 'axios';

export function useDeleteCartItem() {
 const error = ref(null);
 const message = ref(null);

 const deleteCartItem = async (id) => {
    try {
      const response = await axios.delete(`http://localhost:5000/cart/${id}`);
      message.value = response.data.message;
    } catch (err) {
      error.value = err.response ? err.response.data.error : 'An error occurred';
    }
 };

 return { error, message, deleteCartItem };
}

export function useDeleteAllCart() {
  const error = ref(null);
  const message = ref(null);
 
  const deleteAllCartItem = async (username) => {
     try {
       const response = await axios.delete(`http://localhost:5000/cart/${username}`);
       message.value = response.data.message;
     } catch (err) {
       error.value = err.response ? err.response.data.error : 'An error occurred';
     }
  };
 
  return { error, message, deleteAllCartItem };
 }
