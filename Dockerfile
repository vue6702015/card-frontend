# 構建階段
FROM node:20.13-alpine3.20 AS build

# 設置工作目錄
WORKDIR /app

# 複製 package.json 和 package-lock.json 文件
COPY package*.json ./

# 安裝項目依賴
RUN npm ci

# 複製項目文件
COPY . .

# 構建 Vue/Vite 項目
RUN npm run build

# 運行階段
FROM nginx:1.27.0-alpine

# 設置工作目錄
WORKDIR /usr/share/nginx/html

# 複製構建的文件到 Nginx 的靜態文件目錄
COPY --from=build /app/dist .

# 複製自定義的 Nginx 配置文件
COPY nginx.conf /etc/nginx/conf.d/default.conf

# 暴露 Nginx 端口
EXPOSE 80

# 啟動 Nginx
CMD ["nginx", "-g", "daemon off;"]


# # Use the official Node.js image as the base image
# FROM node:20.13-alpine3.20

# # Set the working directory inside the container
# WORKDIR /app

# # Copy the package.json and package-lock.json files
# COPY package*.json ./

# # Install the project dependencies
# RUN npm ci

# # Copy the rest of the project files
# COPY . .

# # Build the Vue/Vite project
# RUN npm run build

# # Expose the port on which the application will run
# EXPOSE 5173

# # Start the application
# CMD ["npm", "run", "start"]
